﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4b
{
    class Order
    {
        public int _number { get; }
        public int _quantity { get; }
        public int _orderDate { get; }

        public Order(int Number, int Quantity, int OrderDate)
        {
            _number = Number;
            _quantity = Quantity;
            _orderDate = OrderDate;

            if (_number < 100 || _number > 999 ||
                _quantity < 1 || _quantity > 12 ||
                _orderDate < 1 || _orderDate > 31)
            {
                if(_number == 0 && _quantity == 0 && _orderDate == 0)
                {
                    DisplayAll();
                }
                throw new ArgumentException();
            }
        }

        public void DisplayAll()
        {
            Console.WriteLine("order number {0} with {1} orders. Ordered on day {2}", _number, _quantity, _orderDate);
            
        }

        
    }
}
