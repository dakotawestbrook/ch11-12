﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4b
{
    class Program
    {
        static void Main(string[] args)
        {
            Order[] orders = new Order[5];

            for (int i = 0; i < orders.Length; i++)
            {
                Console.WriteLine("enter order number for order {0}:", i + 1);
                int number = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("enter order quantity for order {0}:", i + 1);
                int quantity = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("enter order day for order {0}:", i + 1);
                int orderDate = Convert.ToInt32(Console.ReadLine());

                orders[i] = new Order(number, quantity, orderDate);

                
                
            }
            for (int i = 0; i < orders.Length; i++)
            {
                Console.WriteLine("order number {0} with {1} orders. Ordered on day {2}", orders[i]._number, orders[i]._quantity, orders[i]._orderDate);
            }
        }
    }
}
