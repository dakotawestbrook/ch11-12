﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    class MortgageApplication
    {
        public string _name { get; set; }
        public int _creditScore { get; set; }

        public bool Mortgage(double creditScore)
        {
            if(creditScore >= 300 && creditScore <= 850)
            {
                if (creditScore >= 650)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                throw (new ArgumentException());
            }
        }
    }
}
