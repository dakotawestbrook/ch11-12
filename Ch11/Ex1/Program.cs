﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] stuff = new double[20];
            bool end = true;
            int number;
            double result = 0;

            try
            {
                do
                {
                    for (int i = 0; i < stuff.Length; i++)
                    {
                        stuff[i] = i;
                    }
                    Console.WriteLine("Please type an array number my dude");
                    number = Convert.ToInt32(Console.ReadLine());

                    result = stuff[number];

                    end = false;

                } while (end == true);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Your index is out of range man");
            }
            Console.WriteLine("{0}", result);
        }
    }
}
