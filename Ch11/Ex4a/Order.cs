﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4a
{
    class Order
    {
        public int _number;
        public int _quantity;
        public int _orderDate;

        public Order(int Number, int Quantity, int OrderDate)
        {
            _number = Number;
            _quantity = Quantity;
            _orderDate = OrderDate;

            if (_number < 100 || _number > 999 ||
                _quantity < 1 || _quantity > 12 ||
                _orderDate < 1 || _orderDate > 31)
            {
                throw new ArgumentException();
            }
        }

    }
}
