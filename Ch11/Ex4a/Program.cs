﻿  using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4a
{
    class Program
    {
        static void Main(string[] args)
        {
            Order order1 = new Order(100, 2, 15);
            
            Console.WriteLine("order number {0} with {1} orders. Ordered on day {2}", order1._number, order1._quantity, order1._orderDate);
        }
    }
}
