﻿using System;
using System.Windows.Forms;

namespace Ex6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int conversion;

            if(int.TryParse(intText.Text, out conversion))
            {
                display.Text = "Success!";
            }
            else
            {
                display.Text = "Failure!";
            }
        }
    }
}
