﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3
{
    class Program
    {
        static void Main(string[] args)
        {

            double conversion;
            double newNumber;

            Console.WriteLine("Please input a number to have it's square root take:");

            string number = Console.ReadLine();

            if (double.TryParse(number, out conversion))
            {
                newNumber = Convert.ToDouble(number);
                if (newNumber < 0)
                {
                    newNumber = 0;
                    throw (new ArgumentException("Number can't be negative!"));
                }
                else
                {
                    Math.Sqrt(newNumber);
                }
            }
            else
            {
                Console.WriteLine("Invalid entry");
                newNumber = 0;
            }

            Console.WriteLine(Math.Sqrt(newNumber));
        }
    }
}
