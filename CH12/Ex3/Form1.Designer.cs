﻿namespace Ex3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.red = new System.Windows.Forms.RadioButton();
            this.blue = new System.Windows.Forms.RadioButton();
            this.orange = new System.Windows.Forms.RadioButton();
            this.grey = new System.Windows.Forms.RadioButton();
            this.brown = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // red
            // 
            this.red.AutoSize = true;
            this.red.Location = new System.Drawing.Point(29, 35);
            this.red.Name = "red";
            this.red.Size = new System.Drawing.Size(45, 17);
            this.red.TabIndex = 0;
            this.red.Text = "Red";
            this.red.UseVisualStyleBackColor = true;
            this.red.CheckedChanged += new System.EventHandler(this.red_CheckedChanged);
            // 
            // blue
            // 
            this.blue.AutoSize = true;
            this.blue.Location = new System.Drawing.Point(29, 58);
            this.blue.Name = "blue";
            this.blue.Size = new System.Drawing.Size(46, 17);
            this.blue.TabIndex = 1;
            this.blue.TabStop = true;
            this.blue.Text = "Blue";
            this.blue.UseVisualStyleBackColor = true;
            this.blue.CheckedChanged += new System.EventHandler(this.blue_CheckedChanged);
            // 
            // orange
            // 
            this.orange.AutoSize = true;
            this.orange.Location = new System.Drawing.Point(29, 81);
            this.orange.Name = "orange";
            this.orange.Size = new System.Drawing.Size(60, 17);
            this.orange.TabIndex = 2;
            this.orange.TabStop = true;
            this.orange.Text = "Orange";
            this.orange.UseVisualStyleBackColor = true;
            this.orange.CheckedChanged += new System.EventHandler(this.orange_CheckedChanged);
            // 
            // grey
            // 
            this.grey.AutoSize = true;
            this.grey.Location = new System.Drawing.Point(29, 104);
            this.grey.Name = "grey";
            this.grey.Size = new System.Drawing.Size(47, 17);
            this.grey.TabIndex = 3;
            this.grey.TabStop = true;
            this.grey.Text = "Gray";
            this.grey.UseVisualStyleBackColor = true;
            this.grey.CheckedChanged += new System.EventHandler(this.grey_CheckedChanged);
            // 
            // brown
            // 
            this.brown.AutoSize = true;
            this.brown.Location = new System.Drawing.Point(29, 127);
            this.brown.Name = "brown";
            this.brown.Size = new System.Drawing.Size(55, 17);
            this.brown.TabIndex = 4;
            this.brown.TabStop = true;
            this.brown.Text = "Brown";
            this.brown.UseVisualStyleBackColor = true;
            this.brown.CheckedChanged += new System.EventHandler(this.brown_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.brown);
            this.Controls.Add(this.grey);
            this.Controls.Add(this.orange);
            this.Controls.Add(this.blue);
            this.Controls.Add(this.red);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton red;
        private System.Windows.Forms.RadioButton blue;
        private System.Windows.Forms.RadioButton orange;
        private System.Windows.Forms.RadioButton grey;
        private System.Windows.Forms.RadioButton brown;
    }
}

