﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void red_CheckedChanged(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Red;
        }

        private void blue_CheckedChanged(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Blue;
        }

        private void orange_CheckedChanged(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Orange;
        }

        private void grey_CheckedChanged(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Gray;
        }

        private void brown_CheckedChanged(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Brown;
        }
    }
}
