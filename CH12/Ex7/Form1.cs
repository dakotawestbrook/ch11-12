﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.BackColor = System.Drawing.Color.BurlyWood;

            this.button1.BackColor = Color.Coral;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double price = 0;
            if (radioButton1.Checked)
            {
                price = 100;
            }
            else if (radioButton2.Checked)
            {
                price = 150;
            }
            else if (radioButton3.Checked)
            {
                price = 200;
            }

            if (checkBox1.Checked)
            {
                price += 100;
            }
            if (checkBox2.Checked)
            {
                price += 75;
            }
            if (checkBox3.Checked)
            {
                price += 50;
            }

            if (radioButton4.Checked)
            {
                price += 75;
            }
            else if (radioButton5.Checked)
            {
                price += 150;
            }
            else if (radioButton6.Checked)
            {
                price += 300;
            }

            display.Text = price.ToString("C");
        }
    }
}
