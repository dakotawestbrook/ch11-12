﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            fontBox.SetSelected(0, true);
            fontBox.SetSelected(0, true);
        }

        private void submit_Click(object sender, EventArgs e)
        {
            string font ="";
            int size = 0;
            switch (fontBox.SelectedIndex)
            {
                case 0:
                    font = "Times New Roman";
                    break;
                case 1:
                    font = "Symbol";
                    break;
                case 2:
                    font = "Georgia";
                    break;
                case 3:
                    font = "Candara";
                    break;
                case 4:
                    font = "Impact";
                    break;
                default:
                    break;
            }

            switch (sizeBox.SelectedIndex)
            {
                case 0:
                    size = 9;
                    break;
                case 1:
                    size = 10;
                    break;
                case 2:
                    size = 11;
                    break;
                case 3:
                    size = 2;
                    break;
                case 4:
                    size = 20;
                    break;

                default:
                    break;
            }

            hello.Font = new Font(font, size);
        }

        private void fontBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
