﻿namespace Ex5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontBox = new System.Windows.Forms.ListBox();
            this.sizeBox = new System.Windows.Forms.ListBox();
            this.submit = new System.Windows.Forms.Button();
            this.hello = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fontBox
            // 
            this.fontBox.FormattingEnabled = true;
            this.fontBox.Items.AddRange(new object[] {
            "Times New Roman",
            "Symbol",
            "Georgia",
            "Candara",
            "Impact"});
            this.fontBox.Location = new System.Drawing.Point(13, 59);
            this.fontBox.Name = "fontBox";
            this.fontBox.Size = new System.Drawing.Size(120, 95);
            this.fontBox.TabIndex = 0;
            this.fontBox.SelectedIndexChanged += new System.EventHandler(this.fontBox_SelectedIndexChanged);
            // 
            // sizeBox
            // 
            this.sizeBox.FormattingEnabled = true;
            this.sizeBox.Items.AddRange(new object[] {
            "Size 9 font",
            "Size 10 font",
            "Size 11 font",
            "Size 2 font",
            "Size 20 font"});
            this.sizeBox.Location = new System.Drawing.Point(140, 59);
            this.sizeBox.Name = "sizeBox";
            this.sizeBox.Size = new System.Drawing.Size(120, 95);
            this.sizeBox.TabIndex = 1;
            this.sizeBox.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(13, 172);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(120, 35);
            this.submit.TabIndex = 2;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // hello
            // 
            this.hello.AutoSize = true;
            this.hello.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hello.Location = new System.Drawing.Point(149, 181);
            this.hello.Name = "hello";
            this.hello.Size = new System.Drawing.Size(36, 15);
            this.hello.TabIndex = 3;
            this.hello.Text = "Hello";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.hello);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.sizeBox);
            this.Controls.Add(this.fontBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox fontBox;
        private System.Windows.Forms.ListBox sizeBox;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Label hello;
    }
}

