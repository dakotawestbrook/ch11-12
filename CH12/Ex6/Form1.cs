﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void submit_Click(object sender, EventArgs e)
        {
            int price = 0;
            if (radioButton1.Checked)
            {
                price = 10;
            }
            else if (radioButton2.Checked)
            {
                price = 12;
            }
            else if (radioButton3.Checked)
            {
                price = 14;
            }
            else if (radioButton4.Checked)
            {
                price = 17;
            }

            

            int newFeet = Convert.ToInt32(feet.Text);

            display.Text = (price * newFeet).ToString("C");
        }
    }
}
