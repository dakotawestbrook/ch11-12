﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex10
{
    public partial class Form1 : Form
    {
        double price = 0;

        public Form1()
        {
            InitializeComponent();
            monthCalendar1.MinDate = monthCalendar1.TodayDate;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        public void AddUp()
        {
            price = 0;
            if (radioButton1.Checked)
            {
                price += 20;
            }
            else if (radioButton2.Checked)
            {
                price += 25;
            }
            else if (radioButton3.Checked)
            {
                price += 15;
            }

            if (radioButton4.Checked)
            {
                price = price * 0.5;
            }
            else if (radioButton5.Checked)
            {
                price = price * 1;
            }
            else if (radioButton6.Checked)
            {
                price = price * 2;
            }
            else if (radioButton7.Checked)
            {
                price = price * 3;
            }

            displayTotal.Text = price.ToString("C");
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            AddUp();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            const int days = 3;
            dateLabel.Text = "Delivered on " + monthCalendar1.SelectionStart.ToShortDateString() + "\n and will arrive on " + monthCalendar1.SelectionStart.AddDays(days).ToShortDateString(); ;
        }
    }
}
